# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://K9Fenrir@bitbucket.org/K9Fenrir/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/K9Fenrir/stroboskop/commits/30b5e2e3b1547ff0afa45541f1e8530128f34602

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/K9Fenrir/stroboskop/commits/24548773811e9df4f5870729ba1cb5e6665a2e53

Naloga 6.3.2:
https://bitbucket.org/K9Fenrir/stroboskop/commits/17de5ebfe1e671b77f572e22bfb9aa3a782b2be2

Naloga 6.3.3:
https://bitbucket.org/K9Fenrir/stroboskop/commits/d96876bbf343f3d4c0ada7fc14ae96fd8ff70f10

Naloga 6.3.4:
https://bitbucket.org/K9Fenrir/stroboskop/commits/5c0f64dee33dd022df30d6f62f2a2309bfe1b20b

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/K9Fenrir/stroboskop/commits/e7c0c338e702d9c93562e0a0c5eb043fbe81ee97

Naloga 6.4.2:
https://bitbucket.org/K9Fenrir/stroboskop/commits/cad2057f3105a7917b5fd78987dbc07374b9e4c0

Naloga 6.4.3:
https://bitbucket.org/K9Fenrir/stroboskop/commits/47fb5a05eadf6c046c3589d73933f8f51b164258

Naloga 6.4.4:
https://bitbucket.org/K9Fenrir/stroboskop/commits/c416c9a5c78726e1ce28e5f0b00aa1800d919d9d